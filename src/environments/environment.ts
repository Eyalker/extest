// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyDG8W_gp66k634_cIzGqKTDxO4f9fxRXes",
    authDomain: "extest-16709.firebaseapp.com",
    databaseURL: "https://extest-16709.firebaseio.com",
    projectId: "extest-16709",
    storageBucket: "extest-16709.appspot.com",
    messagingSenderId: "1041405979730",
    appId: "1:1041405979730:web:5b26eeb095de89129f838b",
    measurementId: "G-V1CVNE4J7W"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
