import { Component, OnInit } from '@angular/core';
import { ClassifyService } from 'src/app/Services/classify.service';
import { ImageService } from 'src/app/Services/image.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'src/app/Services/auth.service';
import { Injectable } from '@angular/core';

@Component({
  selector: 'app-classified',
  templateUrl: './classified.component.html',
  styleUrls: ['./classified.component.css']
})
export class ClassifiedComponent implements OnInit {

  category:string = "Loading...";
  categoryImage:string; 
  doc:string;
  userID:string;
  // selected:string;

  constructor(public classifyService:ClassifyService,
    public imageService:ImageService,
    public router:Router,
    public authService:AuthService) {}

  ngOnInit() {
    this.classifyService.classify().subscribe(
      res => {
        console.log(res);
        console.log(this.classifyService.categories[res])
        this.category = this.classifyService.categories[res];
        console.log(this.imageService.images[res]);
        this.categoryImage = this.imageService.images[res];
        console.log(this.classifyService.doc)
      }
    )
  }
  saveClassification()
  {
      console.log("userID in saveClassification(): ",this.userID)
      console.log("doc in saveClassification(): ",this.doc)
      console.log("category in saveClassification(): ",this.category)

      //console.log("selected in saveClassification(): ",this.selected)
      
      this.classifyService.addClassification(this.userID,this.doc,this.category);
      console.log("Article added");
      this.router.navigate(['/classified']);
  }
}
