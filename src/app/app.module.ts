import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavComponent } from './Components/nav/nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { AngularFireAnalyticsModule } from '@angular/fire/analytics';
import { AngularFirestoreModule, AngularFirestore } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFireAuthModule, AngularFireAuth } from '@angular/fire/auth';
import { environment } from '../environments/environment';
import { AngularFireModule } from '@angular/fire';
import { LoginComponent } from './Components/login/login.component';
import { SignUpComponent } from './Components/sign-up/sign-up.component';
import { Routes, RouterModule } from '@angular/router';
import { MatExpansionModule} from '@angular/material/expansion';
import { MatFormFieldModule} from '@angular/material/form-field';
import { MatSelectModule} from '@angular/material';
import { MatInputModule} from '@angular/material';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule }   from '@angular/forms';
import { MatCardModule} from '@angular/material/card';
import { SuccessfulComponent } from './Components/successful/successful.component';
import { WelcomeComponent } from './Components/welcome/welcome.component';
import { DocformComponent } from './Components/docform/docform.component';
import { ClassifiedComponent } from './Components/classified/classified.component';







const appRoutes: Routes = [

  { path: 'signup', component: SignUpComponent},
  { path: 'login', component: LoginComponent},
  { path: 'successful', component: SuccessfulComponent},
  { path: 'welcome', component: WelcomeComponent},
  { path: 'classify', component: DocformComponent},
  { path: 'classified', component: ClassifiedComponent},
  { path: "",
    redirectTo: '/welcome',
    pathMatch: 'full'
  },
];
@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    LoginComponent,
    SignUpComponent,
    SuccessfulComponent,
    WelcomeComponent,
    DocformComponent,
    ClassifiedComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    AngularFireAnalyticsModule,
    AngularFirestoreModule,
    AngularFireStorageModule,
    AngularFireAuthModule,    
    AngularFireModule.initializeApp(environment.firebaseConfig),
    RouterModule.forRoot(
      appRoutes,
      // { enableTracing: true } // <-- debugging purposes only
    ),
    MatInputModule,
    MatSelectModule,
    MatFormFieldModule,
    MatExpansionModule,
    HttpClientModule,
    FormsModule,
    MatCardModule,


  ],
  providers: [AngularFirestore,AngularFireAuth],
  bootstrap: [AppComponent]
  
})
export class AppModule { }
